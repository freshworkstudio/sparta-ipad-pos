<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> 
	<html xmlns:v="urn:schemas-microsoft-com:vml">
	<head> 
		<style type="text/css">
			v:* { behavior: url(#default#VML); display: inline-block; }
		</style>
	</head>
	<body>
	
	
<table width='693' height='1455' border='0' cellpadding='0' cellspacing='0'>
  <tr>
    <td width='693' height='521'><div align='left'>
      <table width='693' height='521'  border='0' cellpadding='0' cellspacing='0'>
        <tr>
          <td height='215'><img src='<?php echo $MAIL_ABS_URL; ?>images/mail_top_01.jpg' alt='mail_top' width='693' height='215'></td>
        </tr>
        <tr>
          <td height='30' bgcolor='#000000'><div align='left' style='font-size: 18px;color: #FFFFFF;font-family: Geneva, Arial, Helvetica, sans-serif;font-weight: bold; margin-left:30px;'><?php echo utf8_decode($nombre); ?></div></td>
        </tr>
        <tr>
          <td height='276' style='position:relative' background="<?php echo $MAIL_ABS_URL; ?>images/mail_footer_cotizar.png" style='background:url("<?php echo $MAIL_ABS_URL; ?>images/mail_footer_cotizar.png"); width:693px; height:276px;'>
		  	<!--[if gte vml 1]>
			<v:image style='width: 693px; height: 276px; position: absolute; top: 0; left: 0; border: 0; z-index: -1;' src="".$mainurl."mail/mail_footer_cotizar.png" />
			<![endif]-->
	
			<table><tr><td colspan='2'><img src='<?php echo $MAIL_ABS_URL; ?>images/pixel-182.gif' height='182' width='16' style='height:182px; width:16px' /></td></tr>
			<tr>
			<td width='16'><img src='<?php echo $MAIL_ABS_URL; ?>images/pixel.gif' width='16' height='16' /></td>
			<td>
		  		<div style='color:#fff; font-family:verdana, sans-serif; font-size:12px; line-height:15px;'>
				<strong><?php echo utf8_decode($jefetienda); ?></strong><br/>
				Jefe Tienda Sparta<br />
				<?php echo utf8_decode($nombretienda); ?><br />
				<?php echo utf8_decode($telefonotienda); ?>
				</div>
			</td></tr></table>
		  	<!--<img src='' alt='mail_top04' width='693' height='276'>-->
		</td>
        </tr>
      </table>
    </div></td>
  </tr>
  <tr>
    <td width='693' height='449'><table width='693' height='736' border='0' cellpadding='0' cellspacing='0'>
      <tr>
        <td width='321' height='425'><img src='<?php echo $MAIL_ABS_URL; ?>images/mail_contacto.png' alt='texto'></td>
        <td width='372' height='425'><table width='372' height='424' border='0' cellspacing='0' cellpadding='0'>
          <tr>
            <td width='152' height='173' valign='middle'><div style='font-size: 24px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'><?php echo $nombreproducto; ?></div></td>
            <td width='220'><div align='center'><img src='<?php echo $foto; ?>' alt='Producto' width='170' height='170'></div></td>
          </tr>
          <tr>
            <td height='220' colspan='2' valign='top' background='<?php echo $MAIL_ABS_URL; ?>images/mail_general.jpg' bgcolor='#CACACA'><table width='100%' border='0' cellspacing='5'>
              <tr>
                <td valign='top'>             
                <div align='left' style='font-size: 12px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>
                <?php if($sku != ""){?>
					<p style='font-size: 14px; margin:0; padding:0;display:inline;'><?php echo $sku; ?></p>
				<?php } ?>
				<p style='font-size: 14px; margin:0; padding:0;display:inline;'>Caracteristicas Generales:</p>
				<?php echo $caracteristicas; ?>
				
                </div></td>
              </tr>
              
            </table></td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td height='311' colspan='2'><table width='692' height='282' border='0' cellspacing='5' cellpadding='0'>
          <tr>
            <td width='388' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Descripcion de Articulo:</div></td>
            <td width='93' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Cantidad:</div></td>
            <td width='102' height='20' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Precio Unitario:</div></td>
            <td width='84' bgcolor='#00b9ff'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>Precio:</div></td>
          </tr>
          <tr>
            <td height='247' valign='top' bgcolor='#dfdfdf'><div align='left' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>
         	<?php echo $detalles; ?>
            </div></td>
            <td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>1</div></td>
            <td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>$ <?php echo number_format($precio,0,'.','.'); ?></div></td>
            <td valign='top' bgcolor='#dfdfdf'><div align='right' style='font-size: 14px; color: #000000; font-family: Geneva, Arial, Helvetica, sans-serif; margin:0; padding:0;'>$ <?php echo number_format($precio,0,'.','.'); ?></div></td>
          </tr>
        </table></td>
        </tr>
    </table></td>
  </tr>
  <!--<tr>
    <td width='693' height='485'><a href='http://www.sparta.cl'><img src='http://sparta.resetpixel.com/mail/mail_botton.jpg' alt='sparta' width='693' height='485' border='0'></a>
  </tr>-->
</table>
</body>
</html>