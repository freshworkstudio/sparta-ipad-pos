var init = false;
function App(){
	var me = this;
	this.data = null;
	this.animationtime = 300;
	this.clickevent = "click";
	this.timer = null;
	this.synctimer = null;
	this.syncInterval = 60*1000;
	
	this.init = function(){
		window.init = true;
		//Set variables 
		this.clickevent = (window.navigator.standalone)?"touchstart":"click";
		
		//Preload images
		$.preloadImages("images/loading.gif");

		//Init
		$("[data-ajax]").live("touchstart click",function(){
			initHomeTimeout();
			var url = $(this).attr("href");
			History.pushState(null,null,url);
			return false;
		}); 
		History.pushState(null,null,"home.html");
		if(History.getState().url != ""){
			me.loadUrl(History.getState().url);
		}else{
			//History.pushState(null.null,"home.html");	
		}
		window.onstatechange = function(e){
			initHomeTimeout();
			 me.loadUrl(History.getState().url);
		}
		
		me.triggerTimer();
		
		$(".volver").live(me.clickevent,function(){
			initHomeTimeout();
			History.back();
			return false;
		});
		
		
	}
	this.mainsync = function(){
		if(!navigator.onLine)return false;
		//Notification.show("Sincronizando...",500);
		db.sync(function(synchronized){
			if(synchronized){
				Notification.show("Base de datos sincronizada.",5000);
			}
			db.syncUpload(function(){
				if(synchronized){
					Notification.show("Formularios sincronizados.",5000);
				}
				me.triggerTimer();
			});
			//window.applicationCache.update();
			
		});
		
	}
	this.stopAutoSync = function(){
		clearTimeout(me.synctimer);	
	}
	this.triggerTimer = function(){
		me.synctimer = setTimeout(function(){
			me.mainsync();
		},me.syncInterval);
	}
	
	this.startloading = function(){
		$("#loading").fadeIn(me.animationtime).html("<img src='images/loading.gif' alt='loading' />");
	}
	this.stoploading = function(){
		$("#loading").fadeOut(me.animationtime).html("");
	}
	
	
	
	this.loadUrl = function(url,data){
		me.data = (typeof(data) == "undefined")?null:data;
		me.startloading();
		if(typeof(me.timer) != "undefined" && me.timer != null)clearTimeout(timer);
		var nurl = url;
		var data = {};
		if(url.indexOf("?") != -1){
			nurl =	url.substr(0,url.indexOf("?"));
			  
		}
	
		var xhr = new XMLHttpRequest();
		xhr.open("GET", nurl, true);
		xhr.onreadystatechange = function() {   
			if(xhr.readyState == 4) { 
				var resp = xhr.responseText;
				//window.location.hash =url;
				History.pushState(null,null,url);
				$(".content").fadeOut(300,function(){
					$(this).html(resp).fadeIn(300);
					me.stoploading();
				});
			}
		}
		xhr.send();  
		/*$.post(url,function(resp){
			
		});*/
	}
	
	this.getRequestData = function(){
		//Get request part
		var url = History.getState().url;

		var request = {};
		var pairs = url.substring(url.indexOf('?') + 1).split('&');
		for (var i = 0; i < pairs.length; i++) {
		var pair = pairs[i].split('=');
			request[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
		}	
		return request;
	}
}

function updateOrientation() {
	if(typeof(window.orientation) != "undefined"){
		switch(window.orientation){			
		  case 90: 	  
			$("body").attr("class","landscape");
			$('#portrait').fadeOut(app.animationtime);
			$('.content').fadeIn(app.animationtime);
		  break; 
		  case -90:	  
			$("body").attr("class","landscape");
			$('#portrait').fadeOut(app.animationtime);
			$('.content').fadeIn(app.animationtime);
		  break;  
		  default:
			$("body").attr("class","portrait");
			$('#portrait').fadeIn(app.animationtime);
			$('.content').fadeOut(app.animationtime);
		  break;
		}
	}else{
		$('#portrait').hide();
	}
}
window.addEventListener('orientationchange', function () {
	updateOrientation();
}, true);


$(function(){
	updateOrientation();
});