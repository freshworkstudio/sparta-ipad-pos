<?php

if (!file_exists(CONFIG_PATH)) {
    if ($_SERVER['REQUEST_URI'] != PANEL_URL . "install.php") {
        header("Location: " . PANEL_URL . "install.php");
        exit;
    }
} else {
    include(CONFIG_PATH);
    Conf::setAll();
    if (!@mysql_connect($conf["DB.HOST"], $conf["DB.USER"], $conf["DB.PWD"])) {
        if (strpos($_SERVER['REQUEST_URI'], PANEL_URL . "install.php") === false) {
            header("Location: " . PANEL_URL . "install.php?m=cant_connect");
            exit;
        }
    }
    if (!@mysql_select_db($conf["DB.NAME"])) {
        if (strpos($_SERVER['REQUEST_URI'], PANEL_URL . "install.php") === false) {
            header("Location: " . PANEL_URL . "install.php?m=cant_select_db");
            exit;
        }
    }else{
        mysql_query("SET NAME 'UTF8'");
    }

   /* if(!isset($conf["INSTALLED"]) || !$conf["INSTALLED"]){
        include(INCS_DIR."install.php");
    }*/
}
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

?>
