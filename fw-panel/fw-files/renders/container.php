<?php
$containers = $html->find("fw:container");
if($containers){
	foreach($containers as $container){
		$container->tag = "div";
		$container->class = "content-box";
		
		$container->innertext = '<div class="content-box-header"><h3>'.$container->title.'</h3></div><div class="content-box-content">'.$container->innertext.'</div>';
		$container->title = false;
	}
}

?>