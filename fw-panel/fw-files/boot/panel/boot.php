<?php
$permitidas = array("install.php","install","login","login.php");
if(!$page->check_session() && !in_array($url,$permitidas)){
	
	$redirect = PANEL_URL."login?u=index#!".str_replace("#","%23",$_SERVER['REQUEST_URI']);
	
	if(!$page->put_additional_content){
		die("{redirect_to:'$redirect'}");
	}else{
		header("Location: ".PANEL_URL."login?u=$_SERVER[REQUEST_URI]");
		die('
<script type="text/javascript">
	window.location = "'.PANEL_URL.'login?u="+window.location;
</script>
		');	
	}
}


?>