<?php
class Form{
	static $error;
	static public function create($options){ //$url="", $metodo = "post",$file=false,$onsubmit = ""){
		$defaultForm = array("type" => "normal","action" => $_SERVER['REQUEST_URI'],"method" => "post","onsubmit" => "");
		
		$opts = array_merge($defaultForm,$options);
		$enctype = $opts["type"] == "file"?"multipart/form-data":"application/x-www-form-urlencoded";
		echo "<form action=\"$opts[action]\" method=\"$opts[method]\" enctype=\"$enctype\" onsubmit=\"$opts[onsubmit]\">";
	}
	static public function close($nombre = "", $codigo = ""){
		if($nombre != "")self::submit($nombre,$codigo);
		echo "</form>";
	}
	
	static public function setError($campo,$error){
		$nombre = self::nombre2codigo($campo);
		if($nombre["mod"] === false){
			self::$error[$nombre["code"]][] = $error;
		}else{
			self::$error[$nombre["mod"]][$nombre["code"]][] = $error;
		}
	}
	static public function getError($campo){
		$nombre = self::nombre2codigo($campo);
		$valor = null;
		if($nombre["mod"] === false){
			if(isset(self::$error[$nombre["code"]]))$valor = self::$error[$nombre["code"]];
		}else{
			if(isset(self::$error[$nombre["mod"]][$nombre["code"]]))$valor = self::$error[$nombre["mod"]][$nombre["code"]];
		}
		return $valor[0];
	}
	static public function hasErrors(){
		return (count(self::$error) > 0);
	}
	
	static public function input($options){
		global $data;
		
		ob_start();
		if(empty($options["label"]) && !empty($options["code"]))$options["label"] = ucwords($options["code"]);
		if(!isset($options["echo"]))$options["echo"] = true;
		if(!isset($options["value"])){
			
			if($options["mod"] !== false){
				$valor = (isset($data[$options["mod"]][$options["code"]]))?$data[$options["mod"]][$options["code"]]:null;
			}else{
				$valor = (isset($data[$options["code"]]))?$data[$options["code"]]:null; 	
			}
			if(isset($valor)){
				$options["value"] = (($valor));
			}
		}
		if(isset($options["mod"]) || isset($options["code"])){
			$options["error"] = self::getError(array($options["mod"],$options["code"]));
			$options["name"] = ($options["mod"] !== false)?'data['.$options["mod"].']['.$options["code"].']':'data['.$options["code"].']';
		}else{
			$options["error"] = "";	
			$options["name"] = "";
		}
		$tpl = TEMPLATE_FORM_DIR.$options["type"].".php";
		
		
		include($tpl);
		$res = ob_get_contents();
		ob_end_clean();
		if($options["echo"]){
			echo $res;
		}else{
			return $res;
		}	
	}
	
	static public function text($nombre,$options = array()){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "text";
		return self::input($options);
	}
	static public function date($nombre,$options = array()){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "date";
		return self::input($options);
	}
	static public function password($nombre,$options = array()){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "password";
		return self::input($options);
	}
	static public function textarea($nombre,$options = array()){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "textarea";
		return self::input($options);
	}
	static public function select($nombre,$options){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "select";
		return self::input($options);
	}
	static public function hidden($nombre,$options){
		$options = array_merge(self::nombre2codigo($nombre),$options);
		$options["type"] = "hidden";
		return self::input($options);
	}
	
	static public function submit($value,$options = array()){
		$options["value"] = $value;
		$options["type"] = "submit";
		return self::input($options);
	}
	
	static public function nombre2codigo($nombre){
		if(is_array($nombre) && (isset($nombre["mod"]) && isset($nombre["code"]))){
			return array("mod" => $nombre["mod"],"code" => $nombre["code"]);	
		}
		if(count($nombre) == 2){
			return array("mod" => $nombre[0],"code" => $nombre[1]);	
		}
		$resp = array();
		if(strpos($nombre,".") === false){
			$resp = array("mod" => false,"code" => "$nombre");
		}else{
			list($modulo,$codigo) = (is_array($nombre))?$nombre:explode(".",$nombre);
			$resp = array("mod" => $modulo,"code" => "$codigo");
		}
		return $resp;
	}
	static public function dot2name($nombre){
		$options = self::nombre2codigo($nombre);
		return ($options["mod"] !== false)?'data['.$options["mod"].']['.$options["code"].']':'data['.$options["code"].']';
	}
	static public function valueFromName($nombre){
		global $data;
		$options = self::nombre2codigo($nombre);
		if($options["mod"] !== false){
			$valor = (isset($data[$options["mod"]][$options["code"]]))?$data[$options["mod"]][$options["code"]]:null; 
		}else{
			$valor = (isset($data[$options["code"]]))?$data[$options["code"]]:null; 	
		}
		return $valor;
	}
	
	
	
	
	static public function upload_file($code){
		$name = self::nombre2codigo($code);
		if(!isset($_FILES["data"]))return false;
		$f = $_FILES["data"];
		$file = $name["mod"] != ""?
			array(	"name" 		=> $f["name"][$name["mod"]][$name["code"]],
					"tmp_name" 	=> $f["tmp_name"][$name["mod"]][$name["code"]],
					"size" 		=> $f["size"][$name["mod"]][$name["code"]],
					"error" 	=> $f["error"][$name["mod"]][$name["code"]]):
			
			array(	"name" 		=> $f["name"][$name["code"]],
					"tmp_name" 	=> $f["tmp_name"][$name["code"]],
					"size" 		=> $f["size"][$name["code"]],
					"error" 	=> $f["error"][$name["code"]]);
		if(!is_writable(UPLOADS_DIR)){
			addMessage("El directorio '".UPLOADS_URL."' no tiene permisos de escritura.","error");
			return false;	
		}
		$info = pathinfo($file["name"]);
		$filename = date("Ymdhis").".".rand(1000,9999).".".str_replace(" ","_",substr(substr($info["basename"],0,-(strlen($info["extension"])+1)),0,30)).".".$info["extension"];
		$new_path = UPLOADS_DIR.$filename;
		if(move_uploaded_file($file["tmp_name"],$new_path)){
			return $filename;
		}else{
			addMessage("No se ha completado la transferencia del archivo.","error");
		}
		return false;
	}
	/* 
	function img($nombre, $codigo = NULL,$descripcion="",$editar = true,$eliminar = true){
		global $conf;
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = (isset($_POST[$codigo]))?$_POST[$codigo]:"";
		echo "
		<div class=\"input img\"> 
			<label for=\"\">$nombre:</label>
				<dfn>$descripcion&nbsp;</dfn><div style=\"clear:both\">";
				if($valor != ""){
					echo "<table><tr><td>".Html::img($valor,200,"width=\"100\"")."</td><td>";
						if($eliminar)echo "<input type=\"checkbox\" name=\"elim_{$codigo}\" value=\"true\" />Eliminar imagen<br />";
						if($editar)echo "<input id=\"$codigo\" type=\"file\" name=\"$codigo\">";
						echo "<input name=\"{$codigo}_anterior\" type=\"hidden\" value=\"$valor\" />";
					echo "</td></tr></table>
					";
				}else{
					echo "<strong>No tiene imagen</strong><br /><input id=\"$codigo\" type=\"file\" name=\"$codigo\">";
				}
				echo "
			</div></div>\n";
	}
	function file($nombre, $codigo = NULL,$descripcion="",$editar = true,$eliminar = true){
		global $conf;
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = $_POST[$codigo];
		echo "
		<div class=\"input img\"> 
			<label for=\"\">$nombre:</label>
				<dfn>$descripcion&nbsp;</dfn><div style=\"clear:both\">";
				if($valor != ""){
					echo "<table><tr><td>".Html::link(UPLOAD_DIR.$valor,$valor)."</td><td>";
						if($eliminar)echo "<input type=\"checkbox\" name=\"elim_{$codigo}\" value=\"true\" />Eliminar imagen<br />";
						if($editar)echo "<input id=\"$codigo\" type=\"file\" name=\"$codigo\">";
						echo "<input name=\"{$codigo}_anterior\" type=\"hidden\" value=\"$valor\" />";
					echo "</td></tr></table>
					";
				}else{
					echo "<strong>No tiene imagen</strong><br /><input id=\"$codigo\" type=\"file\" name=\"$codigo\">";
				}
				echo "
			</div></div>\n";
	}
	function pass($nombre,$codigo = NULL,$descripcion = ""){ 
		self::text($nombre,$codigo,$descripcion,"password");
	}
	function text($nombre,$codigo = NULL,$descripcion = "",$type = "text",$extra = ""){
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = (isset($_POST[$codigo]))?$_POST[$codigo]:"";
		echo "
		<div class=\"input text\">
			<label for=\"$codigo\">$nombre:</label>
			<input $extra value=\"".$valor."\" id=\"$codigo\" type=\"$type\" name=\"$codigo\">
			<dfn>$descripcion</dfn>
		</div>
		\n";
	}
	
	function hidden($codigo){
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = (isset($_POST[$codigo]))?$_POST[$codigo]:"";
		echo "<input value=\"$valor\" id=\"$codigo\" type=\"hidden\" name=\"$codigo\">\n";
	}
	
	function textarea($nombre,$codigo = NULL,$descripcion = "",$type = "text"){
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = (isset($_POST[$codigo]))?$_POST[$codigo]:"";
		echo "
		<div class=\"input textarea\">
			<label for=\"$codigo\">$nombre:</label>
			<textarea name=\"$codigo\" id=\"$codigo\">$valor</textarea>
			<dfn>$descripcion</dfn>
		</div>
		\n";
	}
	
	function submit($texto="Enviar datos",$codigo=""){
		if($codigo == "")$codigo = "enviar";
		echo "<div class=\"input submit\"><input type=\"submit\" id=\"$codigo\" name=\"$codigo\" value=\"$texto\"></div>";
	}
	
	function checklist($nombre,$codigo = NULL,$descripcion = "",$listado){
		$codigo = self::escaparCodigo($codigo, $nombre);
		$valor = (isset($_POST[$codigo]))?$_POST[$codigo]:"";
		echo "
		<div class=\"input checklist\">
			<label for=\"\">$nombre</label>
			<dfn>$descripcion</dfn>
			<div class=\"check_lista\" style=\"clear:both;\">"	;
			if(is_array($listado)){
				foreach($listado as $key => $valor){
					$checked = ($valor == $key)?"checked=\"checked\"":"";
					echo "<input $checked type=\"checkbox\" name=\"{$codigo}[]\" value=\"$key\" />$valor";
				}
			}
			echo "</div>";
		echo "</div";
	}
	function radiolist($nombre,$codigo = NULL,$descripcion = "",$listado){
		$codigo = self::escaparCodigo($codigo, $nombre);
		echo "
		<div class=\"input checklist\">
			<label for=\"\">$nombre</label>
			<dfn>$descripcion</dfn>
			<div class=\"check_lista\" style=\"clear:both;\">"	;
			if(is_array($listado)){
				foreach($listado as $valor){
					echo "<input type=\"radio\" name=\"{$codigo}[]\" value=\"$codigo\" />$valor";
				}
			}
			echo "</div>";
		echo "</div";
	}
	function select($nombre,$codigo = NULL,$descripcion = "",$listado){
		$codigo = self::escaparCodigo($codigo, $nombre);
		echo "
		<div class=\"input select\">
			<label for=\"$codigo\">$nombre</label>
			<dfn>$descripcion</dfn>
			<select name=\"$codigo\" id=\"$codigo\">";
			foreach($listado as $key => $valor){
				$selected = (isset($_POST[$codigo]) && $_POST[$codigo] == $key)?"selected = \"selected\"":"";
				echo "<option $selected value=\"$key\">$valor</option>";
			}
			echo "</select>";
			echo "
		</div>";
	}
	function separator($nombre,$descripcion = ""){
		echo "
		<div class=\"input separator\">
			<label>$nombre</label>
			<dfn>$descripcion</dfn>
		</div>";
	}
	
	//UPLOAD
	function upload_file($nombre,$codigo = NULL,$prefijo = ""){
		global $conf;
		$file_name = $_POST["$codigo_anterior"];
		$codigo = self::escaparCodigo($codigo, $nombre);
		if(!isset($_POST["elim_$codigo"])){
			if($_FILES["$codigo"]['name'] != ""){
				$ext = Extension($_FILES["$codigo"]["name"]);
				$file_name = $prefijo.IDUnico(15).$ext;
				if(!move_uploaded_file($_FILES["$codigo"]['tmp_name'],UPLOAD_PATH.$file_name)){
					Mensajes::add("No se pudo cargar el archivo '$nombre'","","alert");
				}
			}
		}else{
			if ($file_name != "")unlink($conf["PHPUPL"].$file_name);
			$file_name = "";
		}
		return $file_name;
	}
	function escaparCodigo($codigo,$nombre){
		if($codigo == NULL)
			$codigo = elimina_acentos(html_entity_decode(strtolower(str_replace(" ","_",$nombre))));
		return $codigo;
	} */
}
?>