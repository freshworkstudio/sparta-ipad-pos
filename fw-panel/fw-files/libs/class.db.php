<?php
class DB{
	static $instance;
	function getInstance(){
		if(self::$instance == null){
			self::$instance = NewADOConnection(Conf::read("DB.TYPE"));
			self::$instance->Connect(Conf::read("DB.HOST"),Conf::read("DB.USER"),Conf::read("DB.PWD"),Conf::read("DB.NAME")) or die("No se pudo conectar con la Base de datos");
		}
		return self::$instance;
	}
}
?>