<?php 
$isEdit = (isset($_GET['id']))?true:false; 
$verb = ($isEdit)?$mod_verb[0]:$mod_verb[1];
$action = ($isEdit)?"save?type=edit&id=$_GET[id]":"save?type=add";
if($isEdit){
	$mod= Model::instance($mod_model);
	$data[$mod_model] = $mod->getById($_GET['id']);	
}

$mod= Model::instance($mod_model);
$mod->returnFormat = "array";
$categorias = $mod->getSimpleArray("nombre");


/***** GET TEMPLATES **/
function getMailTemplates(){
	$mails_template_folder = ABS_DIR."mails/";
	$templates = array();
	$files = array(); 
	$denied = array();
	if ($handle = opendir($mails_template_folder)) { //mails_template_folder -> ABS_DIR.mails/ 
		while (false !== ($file = readdir($handle))) { 
			if (is_dir($mails_template_folder.$file) && substr($file,0,1) != "." && substr($file,0,1) != "_" && !in_array($file,$denied)) { 
				$templates[] = $file; 
			} 
		} 
		closedir($handle); 
	} 
	return $templates;
}
$tmp = getMailTemplates();
$templates = array();
foreach($tmp as $key => $valor){
	$templates[$valor] = $valor;	
}

?>
<fw:title><?php ___("$verb ".strtolower($mod_object)); ?></fw:title>

<fw:container title="Formulario">
	<div class="form">
		<form action="<?php echo $action; ?>" method="post">
		<fw:input type="text" label="Nombre: " name="<?php echo $mod_model; ?>.nombre"></fw:input>
		<fw:input type="image" label="Imagen: " name="<?php echo $mod_model; ?>.imagen"></fw:input>
		<?php echo Form::select("$mod_model.cat_padre",array("default" => "Sin padre","options" => $categorias,"label" => "Categoría padre: ")); ?>
		<?php echo Form::select("$mod_model.mail_template_cotizacion",array("options" => $templates,"label" => "Mail de Cotización: ")); ?>
        <?php echo Form::select("$mod_model.mail_template_confirmacion",array("options" => $templates,"label" => "Mail de Confirmación: ")); ?>

		<div class="input submit">
			<input type="submit" class="button" value="<?php ___("$verb ".strtolower($mod_object)); ?>" />
		</div>
		</form>
	</div>
</fw:container>