<?php
$products = Model::instance($mod_model);
$products->paginate = true;
$products->results_per_page = 20;
if(isset($_GET['q']))$products->search($_GET['q']);

if(isset($_GET['cat']))$products->cond["cat_id"] = $_GET['cat'];
$products->select();
$paginador = $products->paginador(5);	
?>
<style type="text/css">
.disabled td{color:red; text-decoration:line-through; }
</style>

<fw:container id="list" title="Listado de <?php echo strtolower($mod_object_plural); ?>">
	<fw:table fields = "Nombre, Jefe de tienda, Opciones">
		<tfoot>
			<tr><td colspan="10"><?php echo $paginador; ?></td></tr>
		</tfoot>
		<?php
		while($row = $products->each()){ 
			echo "
			<tr>
				<td style='vertical-align:middle'>".$row["Tienda"]["nombre"]."</td>
				<td style='vertical-align:middle'>".$row["Tienda"]["jefe_tienda"]."</td>
				<td style='vertical-align:middle'>
					<div class='reg_option'>
						<a href='registro?id=".$row["Tienda"]["id"]."'>".__("Editar")."</a>
						".delete_btn($row["Tienda"]["id"],"save")."
					</div>
				</td>
			</tr>";
		}
		?>
	</fw:table>
</fw:container>

