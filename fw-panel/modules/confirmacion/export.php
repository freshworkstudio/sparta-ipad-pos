<?php
$filename ="confirmaciones.".time().".xls";
header('Content-type: application/ms-excel');
header('Content-Disposition: attachment; filename='.$filename);


$page->template = false;
if($page->is_ajax){ ?>	
<script>
	window.location = "<?php echo PANEL_URL."confirmacion/export"; ?>";
</script>
<?php exit; }
$products = Model::instance("Confirmacion");
$products->select();
?>

<fw:table fields = "Nombre, Direccion, Comuna, Ciudad, Email, Rut, Genero, Tipo Documento, Numero Documento, Direccion de despacho, Razon Social, Giro, Rut Factura, Teléfono, Cantidad">
<?php
while($row = $products->each()){ 
	echo "
	<tr>
		<td>$row[nombre]</td>
		<td>$row[direccion]</td>
		<td>$row[comuna]</td>
		<td>$row[ciudad]</td>
		<td>$row[email]</td>
		<td>$row[rut]</td>
		<td>$row[genero]</td>
		<td>$row[tipo_documento]</td>
		<td>$row[numero_documento]</td>
		<td>$row[direccion_despacho]</td>
		<td>$row[razon_social]</td>
		<td>$row[giro]</td>
		<td>$row[rut_factura]</td>
		<td>$row[telefono]</td>
		<td>$row[cantidad]</td>
	</tr>";
}
?>
</fw:table>
