<?php 
$isEdit = (isset($_GET['id']))?true:false; 
$verb = ($isEdit)?$mod_verb[0]:$mod_verb[1];
$action = ($isEdit)?"save?type=edit&id=$_GET[id]":"save?type=add";
if($isEdit){
	$mod= Model::instance($mod_model);
	$data[$mod_model] = $mod->getById($_GET['id']);	
}else{
	$data["$mod_model"]["fecha"] = date("Y-m-d");	
}
?>
<fw:title><?php ___("$verb ".strtolower($mod_object)); ?></fw:title>

<fw:container title="Formulario">
	<div class="form">
		<form action="<?php echo $action; ?>" method="post">
		
		<?php echo Form::select("$mod_model.super_destacado",array("options" => array("Destacado normal","Destacado en slider"),"label" => "Tipo de destacado: ")); ?>
		<fw:input type="date" label="Fecha: " name="<?php echo $mod_model; ?>.fecha"></fw:input>
		<?php echo Form::select("$mod_model.color",array("options" => array("light","dark"),"label" => "Color: ")); ?>
		<fw:input type="text" label="URL: " name="<?php echo $mod_model; ?>.url"></fw:input>
		
		<fw:input type="image" label="Thumbnail: " name="<?php echo $mod_model; ?>.thumbnail"></fw:input>
		<fw:input type="image" label="Imagen grande: " name="<?php echo $mod_model; ?>.imagen"></fw:input>
		<fw:input type="text" label="Nombre: " name="<?php echo $mod_model; ?>.nombre"></fw:input>
		<fw:input type="textarea" label="Detalle: " name="<?php echo $mod_model; ?>.detalle"></fw:input>
		
		

		<div class="input submit">
			<input type="submit" class="button" value="<?php ___("$verb ".strtolower($mod_object)); ?>" />
		</div>
		</form>
	</div>
</fw:container>