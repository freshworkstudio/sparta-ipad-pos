<?php
$model = "Subproducto";
$marca = Model::instance($model);

//ELIMINAR
if(isset($_REQUEST['elim'])){
	set_config('lastchange',time());
	if($marca->delete($_REQUEST['elim'])){
		die("{estado:true}");
	}else{
		die("{estado:false}");
	}
}

//ACTIVAR - DESACTIVAR
if(isset($_POST['set_state'])){
	set_config('lastchange',time());
	if($marca->update(array("activo" => $_POST['set_state']),$_GET['id'])){
		$msg = ($_POST['set_state'])?"Desactivar":"Activar";
		die("{estado:true,btn_msg:'$msg'}");
	}else{
		die("{estado:false}");
	}
}


//EDITAR - CREAR
if(isset($data)){
	$msj ="";
	$data[$model]["parent_id"] = $_GET['id'];
	/* REGLAS DE VLIDACION */
	if(trim($data[$model]["parent_id"]) == "")$msj .= "Debe ingresar un producto asociado<br />";
	if(trim($data[$model]["detalle"]) == "")$msj .= "Debe ingresar un nombre<br />";
	if($msj != ""){
		die("{error:1,msg:'<strong>Solucione los siguientes problemas:</strong><br />$msj'}");	
	}
	
	set_config('lastchange',time());
	if(!isset($data[$model]["id"]) || $data[$model]["id"] == ""){
		if($marca->insert()){
			die('addRowSub('.$marca->last_inserted_id.', "'.$data[$model]["detalle"].'", "'.$data[$model]["sku"].'","'.$data[$model]["upc"].'","'.$data[$model]["activo"].'")');
			//die("{msg:'Subproducto creado satisfactoriamente ¿Desea continuar creando?',type:'success',allowsubmit:true,clear:true}");
		}else{
			die("{error:1,msg:'Ocurrió un problema al ingresar los datos.'}");		
		}
	
		exit;
	}else{
		if($marca->update(false, array("Subproducto.id" => $data[$model]['id']) )){
			die("{error:0,msg:'Subproducto editado satisfactoriamente',type:'success',allowsubmit:true}");
		}else{
			die("{error:1,msg:'Ocurrió un problema al editar los datos.'}");		
		}
		exit;	
	}
}

?>