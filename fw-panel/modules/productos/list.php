<?php
$products = Model::instance($mod_model);

$products->order = array("Producto.activo" => "DESC");
$products->paginate = true;
$products->results_per_page = 20;
if(isset($_GET['q']))$products->search($_GET['q']);

if(isset($_GET['cat']))$products->cond["cat_id"] = $_GET['cat'];
$products->select();
$paginador = $products->paginador(5,true,true);	
?>
<style type="text/css">
.disabled td{color:red; text-decoration:line-through; }
</style>


<script type="text/javascript">
$(function(){
	$("#cat_padre").change(function(){
		
		$.post(getTargetUrl("getCategorias",false),{cat_padre:$(this).val()},function(res){
			$("#segundo").html(res);
		});
	});
});
</script>

<fw:container title="Buscador de ficha de producto">
	<div class="form">
		<div class="buscador text">
			<input class="bg_input fw_search" id="buscar_txt" <?php if(isset($_GET['q']))echo "value='$_GET[q]'"; ?> type="text" name="data[Buscar]" />
			<input type="submit" value="Buscar" class="button" id="buscar" />
		</div>
		<div class="">
			<fw:input type="select" style="float:left; width:200px" label="Categoría:" name="Producto.cat_padre">
				<fw:option value='0'>Seleccione una opción</fw:option>
				<?php $mod = Model::instance("Categoria");
				$mod->cond["cat_padre"] = "0";
				$mod->listar("<fw:option value='[id]'>[nombre]</fw:option>");
				?>
			</fw:input>
			<div id="segundo" style="float:left; width:450px;">
				
			</div>
		</div>
	</div>
</fw:container>
<fw:container id="list" title="Listado de <?php echo strtolower($mod_object_plural); ?>">
	<fw:table fields = "Imagen, Nombre, Stock, Opciones">
		<tfoot>
			<tr><td colspan="10"><?php echo $paginador; ?></td></tr>
		</tfoot>
		<?php
		while($row = $products->each()){ 
			$class = $row["Producto"]["activo"]?"":"disabled";
			echo "
			<tr class='$class'>
				<td><img src='".UPLOADS_URL.$row["Producto"]["imagen"]."' width='100' /></td>
				<td style='vertical-align:middle'>".$row["Producto"]["nombre"]."</td>
				<td style='vertical-align:middle'>".$row["Producto"]["stock"]."</td>
				<td style='vertical-align:middle'>
					<div class='reg_option'>
						<a href='registro?id=".$row["Producto"]["id"]."'>".__("Editar")."</a>
						".delete_btn($row["Producto"]["id"],"save")."
						".state_btn($row["Producto"]["id"],$row["Producto"]["activo"],"save")."
					</div>
				</td>
			</tr>";
		}
		?>
	</fw:table>
</fw:container>

