<?php 
$isEdit = (isset($_GET['id']))?true:false; 
$verb = ($isEdit)?$mod_verb[0]:$mod_verb[1];
$action = ($isEdit)?"save?type=edit&id=$_GET[id]":"save?type=add";
if($isEdit){
	$mod= Model::instance($mod_model);
	$data[$mod_model] = $mod->getById($_GET['id']);	
	$data[$mod_model] = $data[$mod_model][$mod_model];
}
$cats = Model::instance("Categoria");
$categorias = $cats->getSimpleArray("nombre");

?>
<style type="text/css">
.disabled td{color:red; text-decoration:line-through; }
</style>

<fw:title><?php ___("$verb ".strtolower($mod_object)); ?></fw:title>

<fw:container title="Formulario">
	<div class="form">
		<form action="<?php echo $action; ?>" method="post">
		<fw:input type="text" label="Nombre: " name="<?php echo $mod_model; ?>.nombre"></fw:input>
		<fw:input type="text" label="Modelo: " name="<?php echo $mod_model; ?>.modelo"></fw:input>
		<fw:input type="text" label="Precio:  (sin puntos ni coma)" name="<?php echo $mod_model; ?>.precio"></fw:input>
		<fw:input type="text" label="Stock: " name="<?php echo $mod_model; ?>.stock"></fw:input>
		<fw:input type="text" label="Video Youtube (código): " name="<?php echo $mod_model; ?>.youtube"></fw:input>
		<fw:input type="image" label="Imagen: " name="<?php echo $mod_model; ?>.imagen"></fw:input>
		<fw:input type="multi_image" label="Más imágenes " name="<?php echo $mod_model; ?>.galeria"></fw:input>
		<fw:input type="textarea" label="Detalle: " name="<?php echo $mod_model; ?>.detalle"></fw:input>
		<fw:input type="textarea" label="Características Técnicas: " name="<?php echo $mod_model; ?>.caracteristicas"></fw:input>
		<?php echo Form::select("$mod_model.activo",array("options" => array("No, estará oculto","Si, será visible"),"label" => "Activo: ")); ?>
		<?php echo Form::select("$mod_model.permitir_compra",array("options" => array("No se podrá comprar","Si, podrá comprarse"),"label" => "Permitir compra: ")); ?>
		
		<?php echo Form::select("$mod_model.cat_id",array("options" => $categorias,"label" => "Categoría: ")); ?>
		
		<div class="input submit">
			<input type="submit" class="button" value="<?php ___("$verb ".strtolower($mod_object)); ?>" />
		</div>
		</form>
	</div>
</fw:container>

<?php if($isEdit) { ?>
<fw:container title="Sub Productos" id="subproductos">
	<fw:table fields = "Detalle, SKU, UPC, Opciones">
		<?php
		$products = Model::instance("Subproducto");

		$products->order = array("Subproducto.activo" => "DESC");		
		if(isset($_GET['id']))$products->cond["parent_id"] = $_GET['id'];
		
		$products->select();
		if($products->num_results == 0)echo "<tr><td colspan=100 style='text-align:center; color:#000;'>- No hay subproductos asociados -</td></tr>";
		while($row = $products->each()){ 
			$class = $row["Subproducto"]["activo"]?"":"disabled";
			echo "
			<tr class='$class'>
				<td style='vertical-align:middle'>".$row["Subproducto"]["detalle"]."</td>
				<td style='vertical-align:middle'>".$row["Subproducto"]["sku"]."</td>
				<td style='vertical-align:middle'>".$row["Subproducto"]["upc"]."</td>
				<td style='vertical-align:middle'>
					<div class='reg_option'>
						<!--<a href='registro?id=".$row["Subproducto"]["id"]."'>".__("Editar")."</a>-->
						".delete_btn($row["Subproducto"]["id"],"save-subproductos")."
						".state_btn($row["Subproducto"]["id"],$row["Subproducto"]["activo"],"save-subproductos")."
					</div>
				</td>
			</tr>";
		}
		?>
	</fw:table>
	
	
	<hr style="margin-top:40px;" />
	<h3>Agregar</h3>
	
	
	<div class="form">
		
		
		<form action="save-subproductos?id=<?php echo $_GET['id']; ?>" method="post">
		<fw:input type="hidden"  name="Subproducto.id"></fw:input>
		<fw:input type="text" label="Detalle: " name="Subproducto.detalle"></fw:input>
		<fw:input type="text" label="SKU: " name="Subproducto.sku"></fw:input>
		<fw:input type="text" label="UPC: " name="Subproducto.upc"></fw:input>
		<?php echo Form::select("Subproducto.activo",array("options" => array("No, estará oculto","Si, será visible"),"label" => "Activo: ")); ?>
		
		
		<div class="input submit">
			<input type="submit" class="button" value="Agregar subproducto" />
		</div>
		</form>
	</div>
</fw:container>
<?php } ?>
<script>
$(function(){
		
});

function addRowSub(id, detalle, sku, upc, state){
		var klass = "";
		var texto_state = "Desctivar";
		if(state == 0){klass="disabled"; texto_state = "Activar"; }
		var new_state = (state==1)?0:1;
		var html = "<tr class='"+klass+"'><td style='vertical-align:middle'>"+detalle+"</td><td style='vertical-align:middle'>"+sku+"</td><td style='vertical-align:middle'>"+upc+"</td><td style='vertical-align:middle'><div class='reg_option'><a href='save-subproductos?elim="+id+"' class='special option_delete'>Eliminar</a><a href='save-subproductos?id="+id+"' state='"+new_state+"' class='special option_state'>"+texto_state+"</a></div></td></tr>";
		$("#subproductos table tbody").append(html);
		return  {"error":false,allowsubmit:true,msg:'Sub producto creado','type':'success'};
}
</script>