<?php
class Subproducto extends MainModel{
    var $name = "Subproducto";
    var $table = "subproductos";
	var $belongsTo = array(
		"Producto" => array(
			"foreignKey"	 => "parent_id"
		)
	);
}
?>