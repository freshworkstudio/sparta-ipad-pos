<?php $page->template = "shadowbox";
$ok = false;
if(!$page->check_session())exit;

if(count($_GET) > 4){
	$info = $_GET;
	$info["html"] = stripslashes($info["html"]);
	$_SESSION['info'] = $info;
}

if(isset($data)){
	$sa = SiteAdmin::getInstance();
	$sa->info = $info;
	$sa->connect();
	$sa->getHtmlParser($info["page"]);
	$markup_selected = $sa->html->find($info["selector"],$info["index"]); 
	$markup_selected->innertext = $data["Contenido"]["valor"];
	$sa->putNewContents();
	$_SESSION['info'] = array();
	unset($_SESSION['info']);
	 ?>
	<script type="text/javascript">
		//parent.location = parent.location;//parent.Shadowbox.close();
		parent.siteAdmin.save("<?php echo $info["selector"]; ?>",<?php echo $info["index"]; ?>,"<?php echo $data["Contenido"]["valor"]; ?>");
		parent.Shadowbox.close();
	</script>
	<?php
	exit;
}else{
	$data["Contenido"]["valor"] = ($info["html"]);
}

//

// A este punto debiesemos estar editando la misma seleccion que hicimos remotamente
//$markup_selected = $sa->html->find($info->selector,$info->index); 
//$markup_selected->innertext = "HOLA";

//$sa->putNewContents();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
body{background:#ccc; font-size:12px; font-family:Verdana, Geneva, sans-serif; color:#333; }
.form .input.textarea textarea{height:15em; width:90%; }
.oneline .input label{text-align:left!important; line-height:26px; width:auto!important;}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
	<h2><?php ___("Editar contenido"); ?></h2>
	<form action="" method="post" class="form oneline">
		<fw:input type="textarea" label="<?php ___("Editar contenido"); ?>" name="Contenido.valor"></fw:input>
		<fw:input type="submit" value="<?php ___("Editar"); ?>"></fw:input>
	</form>
</body>
</html>