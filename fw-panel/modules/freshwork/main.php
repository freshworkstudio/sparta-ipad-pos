<?php

$fw->addReloadJs(PANEL_DIR."resources/js/ajax.reload.js");

//Load Main JS
$fw->addBaseJs(PANEL_DIR."resources/js/jquery.autocomplete.pack.js");
$fw->addBaseJs(PANEL_DIR."resources/js/functions.js"); 
$fw->addBaseJs(PANEL_DIR."resources/js/freshwork.js");
$fw->addBaseJs(PANEL_DIR."resources/js/simpla.jquery.configuration.js");
$fw->addBaseJs(PANEL_DIR."resources/js/swfobject.js");
$fw->addBaseJs(PANEL_DIR."resources/js/facebox.js");
$fw->addBaseJs(PANEL_DIR."resources/js/jquery.hashhistory.js");
$fw->addBaseJs(PANEL_DIR."resources/js/uploadify/jquery.uploadify.js");
$fw->addBaseJs(PANEL_DIR."resources/js/jquery.ui.min.js");
$fw->addBaseJs(PANEL_DIR."resources/js/ajax.js");
$fw->addBaseJs($fw->getReloadJsFilename(true));
$fw->addBaseJs(PANEL_DIR."resources/js/freshwork2.js");


$fw->addBaseCss(PANEL_CSS_DIR."reset.css");
$fw->addBaseCss(PANEL_CSS_DIR."style.css");
$fw->addBaseCss(PANEL_CSS_DIR."invalid.css");
$fw->addBaseCss(PANEL_CSS_DIR."uploadify.css");
$fw->addBaseCss(PANEL_CSS_DIR."custom-theme/style.css");
$fw->addBaseCss(PANEL_CSS_DIR."jquery.autocomplete.css");

?>