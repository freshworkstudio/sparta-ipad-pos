<?php
$recetas = Model::instance("Mod");
$recetas->paginate = true;
$recetas->results_per_page =8;
$recetas->select();
$paginador = $recetas->paginador(5);
?>
<fw:table mod="Receta" fields = "Nombre,C&oacute;digo, Opciones">
	<tfoot>
		<tr><td colspan="10"><?php echo $paginador; ?></td></tr>
	</tfoot>
	<?php
	while($row = $recetas->each()){ 
		echo "
		<tr reg='$row[mod_id]'>
			<td>$row[mod_name]</td>
			<td>$row[mod_code]</td>
			<td>
				<div class='reg_option'>
					".edit_btn($row["mod_id"],"mods/edit")."
					<a href='".$page->module_url."edit_campos?id=".$row["mod_id"]."'>Editar campos</a>
					".delete_btn($row["mod_id"],"mods/save")."
				</div>
			</td>
		</tr>";
	}
	?>
</fw:table>

