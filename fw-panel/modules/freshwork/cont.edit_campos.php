<?php
$module = Model::instance("module");
$module = $module->getById($_GET['id']);

$f = Model::instance("field");
$f->cond["mod_id"] = $_GET['id'];
$fields = $f->select("*","array");

$data["Field"]["mod_id"] = $_GET['id'];

$types = array(
	"TXT"		=> __("Texto simple"),
	"LTXT"		=> __("Texto largo"),
	"IMG"		=> __("Imagen"),
	"MIMG"		=> __("M&uacute;ltiples im&aacute;genes"),
	"REL"		=> __("Relaci&oacute;n")
);
?>