<?php

$frases = array();
$frases[] = __("If you want that girls love you, use Freshwork with one of these browsers.");
$frases[] = __("Upgrade to one of these browsers to have superpowers.");
$frases[] = __("Use Freshwork. Eso.");
$frases[] = __("No sea pollo. Actualize su navegador.");
?>
<script type="text/javascript">
	<?php $frases_js = array(); foreach($frases as $frase)$frases_js[] = "'$frase'"; ?>
	var quotes = [<?php echo implode($frases_js,","); ?>],currentQuote = 0;
	$(function(){
		seeQuote();
	});
	function seeQuote(){
		if (++currentQuote == quotes.length)currentQuote = 0;
		$(".quotes quote").fadeOut(function(){$(this).html(quotes[currentQuote]).fadeIn()	;})
		setTimeout(function(){
			seeQuote();
		},5000);
	}
</script>
<style type="text/css">
	.browsers{overflow:hidden; }
	.browsers, .browsers li{list-style:none; margin:0 !important; padding:0 !important; background:none  !important; }
	.browsers li{width:25%; text-align:center; float:left; }
	.browsers li span{display:block; height:100%; font-size:18px; margin-bottom:10px; }
	.browsers li img{width:70%; }
	.browsers li .button{width:50%; margin:0 auto; }
	.quotes{text-align:center; margin-top:60px; }
	quote{margin-top:10px; font-size:25px;  }
	.iexplorer{overflow:hidden; margin-top:30px;float:left;}
	.iexplorer img{width:40px; float:left; }
	.iexplorer p{float:left; display:block; font-size:90%; margin:0 0 0 10px; padding:0; }
	.iexplorer p a{ line-height:12px; clear:both; float:left; }
</style>
<fw:container title="<?php ___("Upgrade with the option that best suits you."); ?>">
	<ul class="browsers">
		<li>
			<span>Google Chrome</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Chrome.png" />
			<a target="_blank"  href="http://www.google.com/chrome" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Firefox</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Firefox.png" />
			<a target="_blank"  href="http://www.mozilla.com/firefox/" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Safari</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Safari.png" />
			<a target="_blank"  href="http://www.apple.com/safari" class="noajax button"><?php ___("Download"); ?></a>
		</li>
		<li>
			<span>Opera</span>
			<img src="<?php echo RESOURCES_URL; ?>images/browsers/Opera.png" />
			<a target="_blank" href="http://www.opera.com/download/" class="noajax button"><?php ___("Download"); ?></a>
		</li>
	</ul>
	<div class="quotes">
		<quote></quote>
	</div>
	<div class="iexplorer">
		<img src="<?php echo RESOURCES_URL; ?>images/browsers/IE.png" />
		<p><?php ___("Si lo deseas, también puedes descargar la última versión de Internet Explorer"); ?>
		<a target="_blank" href="http://windows.microsoft.com/en-us/internet-explorer/products/ie/home" class="noajax button"><?php ___("Download"); ?></a></p>
	</div>
</fw:container>