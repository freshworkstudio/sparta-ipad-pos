<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/reset.css" type="text/css" media="screen" />
	<!-- Main Stylesheet -->
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/style.css" type="text/css" media="screen" />
	<link rel="stylesheet" href="<?php echo PANEL_URL; ?>resources/css/invalid.css" type="text/css" media="screen" />
	<style type="text/css">
		html{margin:0 !important; padding:0 !important; }
		body{margin:0; padding:0; }
		#shadow_wrapper{padding:20px; }
	</style>
</head>
	
<body>
	<div id="shadow_wrapper">
		<?php echo $page->body; ?>
	</div>
</body>
</html>