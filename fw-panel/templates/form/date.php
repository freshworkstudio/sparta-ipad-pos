<?php if(isset($options["value"]) && $options["value"] == "0000-00-00 00:00:00")unset($options["value"]); ?>
<div class="input text date<?php if(isset($options["class"]))echo $options["class"]; ?>">
	<label for="<?php echo $options["code"]; ?>"><?php echo $options["label"]; ?></label>
	<input type="text" id="<?php echo $options["code"]; ?>_alt" value="<?php if(isset($options["value"]))echo date("d-m-Y",strtotime(($options["value"]))); ?>"  />
	<input type="hidden" name="<?php echo $options["name"]; ?>" class="altField" id="<?php echo $options["code"]; ?>" value="<?php if(isset($options["value"]))echo substr(($options["value"]),0,10); ?>" />
	<a href="#" class="clear"><?php ___("Clear"); ?></a>
	<?php if(isset($options["error"]) && $options["error"] != ""){ ?>
		<span class="error"><?php echo $options["error"]; ?></span>
	<?php } ?>
	<?php if(isset($options["desc"]) && $options["desc"] != ""){ ?>
		<small><?php echo $options["desc"]; ?></small>
	<?php } ?>
</div>