<div class="input select">
	<label for="<?php echo $options["code"]; ?>"><?php echo $options["label"]; ?></label>
	<select name="<?php echo $options["name"]; ?>" id="<?php echo $options["code"]; ?>">
		<?php if(isset($options["default"]) && $options["default"] != false){ ?>
			<option value=""><?php echo $options["default"]; ?></option>
		<?php } ?>
		<?php foreach($options["options"] as $key => $value){
			$selected = (isset($options["value"]) && $options["value"] == $key)?'selected="selected"':"";
			echo "<option $selected value=\"$key\">".($value)."</option>";
		} ?>
	</select>
	<?php if(isset($options["error"]) && $options["error"] != ""){ ?>
		<span class="error"><?php echo $options["error"]; ?></span>
	<?php } ?>
	<?php if(isset($options["desc"]) && $options["desc"] != ""){ ?>
		<small><?php echo $options["desc"]; ?></small>
	<?php } ?>
</div>