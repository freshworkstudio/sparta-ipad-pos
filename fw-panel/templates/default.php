<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>FreshWork Admin - The Simplest Admin</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<!--  CSS  -->
	<?php echo $fw->putBaseCss(); ?>

	<!-- Internet Explorer Fixes Stylesheet -->
	<!--[if lte IE 7]>
			<link rel="stylesheet" href="resources/css/ie.css" type="text/css" media="screen" />
		<![endif]-->
	<!--[if IE 6]>
			<link rel="stylesheet" href="resources/css/ie6.css" type="text/css" media="screen" />
	<![endif]-->

	<?php echo $fw->putBaseJs(); ?><!-- <script type='text/javascript' src='fw-files/tmp/panel_base_js.js'></script>-->
	
	<?php echo $page->head; ?>
</head>
<body>
<div id="ajax_loading">
	<img src="<?php echo PANEL_URL; ?>resources/images/loading3.gif" alt="Cargando..." />
	<span>Cargando...</span>
</div>
<div id="body-wrapper">
	<!-- Wrapper for the radial gradient background -->
	<?php Component::load("sidebar"); ?>
	<!-- End #sidebar -->
	<div id="main-content">
		<noscript>
		<!-- Show a notification if the user has disabled javascript -->
		<div class="notification error png_bg">
			<div> <?php ___("Javascript disabled","choose_browser","http://www.google.com/support/bin/answer.py?answer=23852"); ?></div>
		</div>
		</noscript>
		<!-- CONTENIDO DEL SITIO -->
		<?php echo $page->body; ?>
		<!-- FIN CONTENIDO SITIO -->
		<!--<div id="footer"> <small> &#169; Copyright 2009 | Powered by <a href="http://www.freshworkstudio.com">FreshWork Studio</a> | <a href="#">Top</a> </small> </div>-->
		<!-- End #footer -->
	</div>
	<!-- End #main-content -->
</div>
</body>
</html>