// JavaScript Document
current_url = getCurrentUrl()
$(function(){
	$('.file [type=file]').each(function(){
		var obj = this;
		var $input = $(obj).closest(".input");
		var $img_div = $input.find(".img");
		var queID = false;
		//Documentation: http://www.uploadify.com/documentation/
		
		$input.find(".delete").click(function(){
			if(!confirm("¿Esta seguro que desea eliminar este archivo?"))return false;
			
			$input.find("input").val("");
			$input.find(".current").fadeOut();
			$input.find(".reset").fadeIn();
			$input.find(".img img").fadeOut();
			$(this).fadeOut();
			
			return false;
		});
		$input.find(".reset").click(function(){
			if(queID != false)$(obj).uploadifyClearQueue();
			cancelImage($input);
			return false;
		});
		
		$input.data("first_image",$img_div.find("img").attr("src"));
		if($(this).data("uploadified") !== "true")$(this).data("uploadified","true").uploadify({
			'uploader'		: 'resources/js/uploadify/uploadify.swf',
			'script'		: 'fw-files/standalone/uploader.php',
			'folder'		: 'fw-files/fw-uploads/',
			'cancelImg'		: 'resources/js/uploadify/cancel.png',
			"wmode"			: "transparent",
			"auto"			: true,
			"onSelect"		: function(e){
				$(e.target).parent().find("object").css("height","0px");
				uploading_files++;
			},
			"onComplete"	: function(e,ID,file,response,data){ //http://www.uploadify.com/documentation/events/oncomplete-2/
				var resp = eval("("+response+")");
				queID = ID;
				if(resp.estado){
					var fileId = $(e.target).attr("id");
					$("#"+fileId+ID).find(".percentage").text(" - Finalizado");
					$("#"+fileId+ID).addClass("complete");
					$("#"+fileId+"Value").val(resp.filename);
					finalizaArchivo("complete");
					if($input.is(".image")){ //Si es uun input del tipo imagen
						var img = $("<img />").attr("src",UPLOADS_URL+resp.filename);
						
						
						$img_div.find("img").hide(100,function(){
							$img_div.html("<span class='loading'>Cargando...</span>");
							//$img_div.html(img);
							//img.hide().fadeIn(1000);
						});
						
						
						img[0].onload = function(){
							$img_div.find(".loading").hide();
							$img_div.html(img);
							img.fadeIn(1000);
							$input.find(".current .filename").html(resp.filename);
							
						}
						//img.fadeIn();
						$input.find(".delete").hide();
						$input.find(".reset").show();
					}
				}else{
					addMessage(resp.msg,"error",$(e.target).closest("form")[0]);	
				}
				return false;
			},
			"onCancel"		: function(e){
				cancelImage($input);
				return false;
			},
			"onError"		: function(a,b,c,d){
				finalizaArchivo("error");
			}
		});
		function cancelImage($input){
			var fname = $input.data("first_image");
			var fileId = $input.find("input").attr("id");
			$input.find("object").css("height","30px");
			$input.find(".reset").hide();
			$("#"+fileId+"Value").val("");
			finalizaArchivo("cancel");
			if(fname != null){
				$img_div.find("img").attr("src",fname).fadeIn();	
				$input.find(".current").fadeIn().find(".filename").html(fname);
				$input.find(".delete").show();	
			}else{
				$img_div.find("img").hide(500);
				$input.find(".delete").hide();
			}
		}
	});
	
	$("table th[order]").each(function(){
		$(this).addClass("order");
	});
	
	$(".file input").each(function(){
		if($(this).parent().find("[type=hidden]").length <= 0){
			var hidden = $("<input>").attr("type","hidden").attr("name",$(this).attr("name")).attr("id",$(this).attr("id")+"Value");
			$(this).parent().append(hidden);
		}
	});

	$(".multiselect .multiselect_text").each(function(){
		var obj = this;
		$(this).autocomplete($(this).attr("url"), {
			width: 300,
			matchContains: true,
			formatItem: formatItem,
			formatResult: formatResult,
			matchCase:true
		});
		if($(this).attr("include") != ""){
			var container = $(obj).parent().parent().find(".multiselect_options");
			container.html("Cargando datos...");
			SoloEnviar($(this).attr("url"),"ids="+$(this).attr("include"),function(con){
				var tmp = con.responseText.split("\n");
				container.html("");
				var new_div, tmp2;
				for(j in tmp){
					tmp2 = tmp[j].split("|");
					key = tmp2[0];
					name = tmp2[1];	
					if(key != ""){
						new_div = $("<div class='option'>");
						new_div.html(name+"<input type='checkbox' checked='checked' name='"+container.attr("name")+"[]' value='"+key+"' /><span class='close'></span>");
						container.append(new_div);
					}
				}
			});
		}
	}).result(function(event, data, formatted) {
		this.value = "";
		var key = data[0]
		var name = data[1]
		//$(this).next().val($(this).next().val()+formatted+",");
		var container = $(this).parent().parent().find(".multiselect_options");
		var new_div = $("<div class='option'>");
		new_div.html(name+"<input type='checkbox' checked='checked' name='"+container.attr("name")+"[]' value='"+key+"' /><span class='close'></span>");
		container.prepend(new_div);
		//hidden.val( (hidden.val() ? hidden.val() + ";" : hidden.val()) + data[1]);
	});

	if($(".input.html textarea").length > 0)$(".input.html textarea").cleditor({
		width:"99%"
	});
	
	//Set Datepicker
	$(".input.date input[type=text]").each(function(){
		var obj = this;
		var $input = $(obj).closest(".input");
		$input.find(".clear").click(function(){
			$input.find('.altField').val("");
			$(obj).val("");
			return false;
		});
		var yearRange = (typeof($input.attr("yearRange")) != "undefined")?$input.attr("yearRange"):"c-100:c+10";
		$(this).datepicker({'dateFormat':'dd-mm-yy',"changeYear":true,changeMonth:true,"firstDay":1,"yearRange":yearRange,"altFormat":'yy-mm-dd',"altField":$input.find('.altField')});
	});
	//Prevent that URL problems when selecting a date
	$(".ui-datepicker a").live("click",function(){
		return false;
	});
	
	if($('ul.content-box-tabs li a.current').length <= 0){
		$('.content-box .content-box-content div.tab-content').hide(); // Hide the content divs
		var tab = getCookie("current-tab");
		var tmp = $("ul.content-box-tabs li [href="+tab+"]");
		if(tmp.length > 0){
			tmp.click();
		}else{
			$('ul.content-box-tabs li a:eq(0)').click(); // Add the class "current" to the default tab
		}
		if($('.content-box-content div.tab-content:visible').length <= 0)$('.content-box-content div.tab-content:eq(0)').show();
	}
		
	$('tbody tr:even').addClass("alt-row"); // Add class "alt-row" to even table row
	// Check all checkboxes when the one in a table head is checked:
	
	
	// Initialise Facebox Modal window:
	$('a[rel*=modal]').facebox(); // Applies modal window to any link with attribute rel="modal"
	
	// Initialise jQuery WYSIWYG:
	//$(".wysiwyg").wysiwyg(); // Applies WYSIWYG editor to any textarea with the class "wysiwyg"
	
});