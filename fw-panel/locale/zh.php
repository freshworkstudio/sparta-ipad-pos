<?php
$lang_name = "Chino";
$lang = array(
	"Instalación de FreshWork"		=> "Instalación de FreshWork Admin",
	"Is it time to change the site?"=> "是时候改变网站？",
	"Hello, %s"						=> "您好, %s",
	"View the Site"					=> "查看本网站",
	"Choose Web Browser"			=> "更新浏览器",
	"Sign Out"						=> "登出",
	"Sign in"						=> "登录",
	"User"							=> "用户",
	"Password"						=> "密码",
	"Download"						=> "下载",
	/* Nuevo producto */
	"New Product"					=> "Nuevo poducto",
	"Edit Product"					=> "Editar Producto",
	"It has never been so easy to edit products."	=> "Nunca fué tan fácil administrar productos",
	
	//Paginador
	"<< Comienzo"					=> "<<",
	"< Anterior"					=> "<",
	"Siguiente >"					=> ">",
	"Final >>"						=> ">>",
);
?>