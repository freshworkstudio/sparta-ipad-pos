<?php
$lang_name = "Espa&ntilde;ol";
$lang = array(
	"Instalación de FreshWork"		=> "Instalación de FreshWork Admin",
	"Is it time to change the site?"=> "¿Es momento de modificar el sitio?",
	"Hello, %s"						=> "Hola, %s",
	"View the Site"					=> "Ver el sitio",
	"Choose Web Browser"			=> "Actualizar navegador web",
	"Sign Out"						=> "Cerrar sesión",
	"Sign in"						=> "Iniciar sesión",
	"User"							=> "Usuario",
	"Password"						=> "Contraseña",
	"Download"						=> "Descargar",
	/* Nuevo producto */
	"New Product"					=> "Nuevo poducto",
	"Edit Product"					=> "Editar Producto",
	"It has never been so easy to edit products."	=> "Nunca fué tan fácil administrar productos",
	
	"<< Comienzo"					=> "<<",
	"< Anterior"					=> "<",
	"Siguiente >"					=> ">",
	"Final >>"						=> ">>",
	
	
	"Javascript disabled"			=> "Javascript está deshabilitado o tu navegador no lo soporta. Por favor, <a href=\"%s\" title=\"Actualiza tu navegador por uno mejor.\">actualiza tu navegador</a> o <a href=\"%s\" title=\"¿Como habilitar Javascript\">habilita javascript</a> para poder usar el sistema.",
	
	/* Form */
	"Error trying to upload the file."	=> "Error tratando de subir el archivo.",
	"Clear"								=> "Limpiar",
	
	/* Choose Browsers */
	"Upgrade with the option that best suits you."=> "Actualiza con la opción que más te acomode.",
	"Upgrade to one of these browsers to have superpowers." => "Actualiza a uno de estos navegadores para tener superpoderes.",
	"Chuck Norris uses Freswork with one of these browsers." => "Chuck Norris utiliza Freshwork con uno de estos navegadores.",
	"If you want that girls love you, use Freshwork with one of these browsers." => "Si quieres que las chicas te amen, usa Freshwork con uno de estos navegadores."	
);
?>