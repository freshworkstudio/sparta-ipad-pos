<?php
$page->template = false;
$page->put_additional_content = false;

if((int)get_config("lastchange") > (int)$_POST['lastsync']){
	
	//Categorias
	$mod = Model::instance("Categoria");
	$mod->returnFormat="array";
	$tmp= $mod->select();
	$cats = array();
	foreach($tmp as $key => $valor){
		$cats[$valor["id"]] = array(
			"id" 			=> $valor["Categoria"]["id"],
			"cat_padre" 	=> $valor["Categoria"]["cat_padre"],
			"nombre" 		=> $valor["Categoria"]["nombre"],
			"imagen" 		=> $valor["Categoria"]["imagen"],
			"activo"		=> $valor["Categoria"]["activo"]
		);
	}	
	
	//Productos
	$mod = Model::instance("Producto");
	$mod->returnFormat="array";
	$tmp= $mod->select();
	$productos = array();
	foreach($tmp as $key => $valor){
		$productos[$valor["Producto"]["id"]] = array(
			"id" 				=> $valor["Producto"]["id"],
			"cat_id" 			=> $valor["Producto"]["cat_id"],
			"nombre" 		=> $valor["Producto"]["nombre"],
			"modelo" 		=> $valor["Producto"]["modelo"],
			"imagen" 		=> $valor["Producto"]["imagen"],
			"galeria" 		=> $valor["Producto"]["galeria"],
			"detalle" 		=> $valor["Producto"]["detalle"],
			"caracteristicas" => $valor["Producto"]["caracteristicas"],
			"precio" 		=> $valor["Producto"]["precio"],
			"stock" 			=> $valor["Producto"]["stock"],
			"youtube" 		=> $valor["Producto"]["youtube"],
			"activo"			=> $valor["Producto"]["activo"],
			"permitir_compra"		=> $valor["Producto"]["permitir_compra"]
		);
	}
	
	//Tiendas
	$mod = Model::instance("Tienda");
	$mod->returnFormat="array";
	$tmp= $mod->select();
	$tiendas = array();
	foreach($tmp as $key => $valor){
		$tiendas[$valor["Tienda"]["id"]] = array(
			"id" 				=> $valor["Tienda"]["id"],
			"nombre" 			=> $valor["Tienda"]["nombre"],
			"jefe_tienda" 		=> $valor["Tienda"]["jefe_tienda"],
			"telefono" 			=> $valor["Tienda"]["telefono"],
			"direccion" 		=> $valor["Tienda"]["direccion"],
			"mails_confirmacion"=> $valor["Tienda"]["mails_confirmacion"],
			"mails_cotizacion" 	=> $valor["Tienda"]["mails_cotizacion"],
			"password"			=> $valor["Tienda"]["password"]
		);
	}	
	//SubProductos
	$mod = Model::instance("Subproducto");
	$mod->returnFormat="array";
	$tmp= $mod->select();
	$subproductos = array();
	foreach($tmp as $key => $valor){
		$subproductos[$valor["Subproducto"]["id"]] = array(
			"id" 				=> $valor["Subproducto"]["id"],
			"parent_id" 	=> $valor["Subproducto"]["parent_id"],
			"detalle" 		=> $valor["Subproducto"]["detalle"],
			"upc" 				=> $valor["Subproducto"]["upc"],
			"sku" 				=> $valor["Subproducto"]["sku"],
			"activo" 			=> $valor["Subproducto"]["activo"]
		);
	}	
	
	echo json_encode(array(
		"categorias" 	=> $cats,
		"productos"		=> $productos,
		"subproductos" => $subproductos,
		"tiendas"		=> $tiendas,
		"pass"			=> get_config("password_envio"),
		"lastchange"	=> get_config("lastchange"),
		"synced"		=> true
	));
}else{
	echo json_encode(array("synced" => false));	
}
?>